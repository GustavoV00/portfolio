import React from "react";

// import { Container } from './styles';

function Main() {
  return (
    <main>
      <div className="main-content">
        <h3 className="main-content__my-name">Hello Everyone!</h3>
        <h2 className="main-content__gvn">
          I'm Gustavo Valente, <br />
          <span className="main-content__software-developer">
            Software Developer
          </span>
        </h2>

        <div>
          <p className="main-content__descri">
            I'm a Software Engineering student... keep written something
          </p>
          <button className="main-content__btn-resume">
            Check out my resume!
          </button>
        </div>
      </div>
      <div className="icons">
        ICON 1 <br />
        ICON 2 <br />
        ICON 3 <br />
      </div>
    </main>
  );
}

export default Main;

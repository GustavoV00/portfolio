import React from "react";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faGithub,
  faInstagram,
  faLinkedin,
} from "@fortawesome/free-brands-svg-icons";

// import { Container } from './styles';

function Header() {
  return (
    <header className="header">
      <span className="header__logo">LOGO</span>
      <nav className="nav">
        <ul className="nav__ul">
          <li className="nav-li__margin">About</li>
          <li className="nav-li__margin">Skills</li>
          <li className="nav-li__margin">Projects</li>
          <li className="nav-li__margin">Blog</li>
          <li className="nav-li__margin nav-contact">Contact</li>
        </ul>
      </nav>

      {/* <section className="icons">
        <span className="icons__ic icons__mg-right">
          <FontAwesomeIcon icon={faInstagram} />
        </span>
        <span className="icons__ic icons__mg-right">
          <FontAwesomeIcon icon={faLinkedin} />
        </span>
        <span>
          <FontAwesomeIcon className="icons__ic" icon={faGithub} />
        </span>
      </section> */}
    </header>
  );
}

export default Header;
